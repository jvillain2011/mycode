variable "container_name" {
  description = "Value of the name for the Docker container"
  # basic types include string, number and bool
  type        = string
  default     = "ExampleNginxContainer"
}
variable "int_port"{
  description = "Value of the internal port for the docker container"
#basic type is set to number with defaults defined from the example
type = number
default = "80"
}

variable "ext_port"{
description = "Vaule of the external port for the docker container"
type = number
default = "2224"
}
