variable "container_name" {
   default = "AltaResearchWebService"
}

variable "internal_port" {
   description = "internal port used"
   type = number
   default = 9876
}

variable "external_port" {
   description =  "external port used"
   type = number
   default = 5432
}
