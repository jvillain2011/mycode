variable "container_name" {
   default = "AltaResearchWebService"
}

variable "int_port" {
   description = "internal port used"
   type = number
   default = 9876
}

variable "ext_port" {
   description = "external port used"
   type = number
   default = 5432
}







